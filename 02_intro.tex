Imaging from large stand-off distances typically results in low spatial resolution.
While imaging a distant target, diffraction blur, caused by the limited angular extent of the input aperture, is the primary cause of resolution loss.
As a consequence, it is desirable to use a large lens. 
However, telephoto lenses of comparable f-numbers ($f/\#$s) are typically an order of magnitude more expensive and heavier than their portrait counterparts.
For example, consider imaging a subject at a distance of $z=1$ km with a lens aperture diameter of $d = 12.5$ mm. 
Then the diffraction spot size is $\lambda z/d \approx 50$ mm diameter diffraction blur size at the object, completely blurring out important features, such as faces. 
If a large lens (same focal length but with a $125$ mm-wide aperture) is used instead, the diffraction blur is reduced to a $5$ mm diameter, which is small enough to be enable face recognition from $1$ km away.
Unfortunately, such telescopic lenses are expensive and bulky, and are thus rarely used.

\begin{figure}[t!]%
\centering
\includegraphics[width=\linewidth]{diffractionLimited}%
\caption{\textbf{Using active illumination to overcome the diffraction limit in long range imaging.} Diffraction blur is the primary cause of resolution loss in long distance imaging. Consider this illustration of imaging a human-sized object $1$ km away.
(a) A conventional camera using passive illumination with a fixed aperture size of $12.5$ mm induces a diffraction spot size of $50$ mm on objects $1$ km away, destroying relevant image features. (b) Using Fourier ptychography, an array of cameras using coherent illumination creates a synthetic aperture $10$ times larger than (a) resulting in a diffraction spot size of $5$ mm for scenes $1$ km away. Phase retrieval algorithms recover the high resolution image. (c) In this paper, we present experimental results captured using a table-top prototype which can be used to simulate many of the properties of the desired system in (b).}%
\label{fig:diffractionLimited}%
\vspace{-0.1in}
\end{figure}

\begin{figure*}%
\includegraphics[width=\linewidth]{MotiveFigure}%
\caption{\textbf{Diffraction blur limits in long distance imaging.} (a) Diffraction blur spot size as a function of distance for common and exotic imaging systems. Consumer cameras (shaded in the pink region) are designed for close to medium range imaging but cannot resolve small features over great distances. Professional lenses (green region) offer improved resolution but are bulky and expensive. Identifying faces $1$ kilometer away is possible only with the most advanced super telephoto lenses, which are extremely rare and cost upwards of $\$2$ million US dollars \cite{walkenhorst2012world}. Obtaining even finer resolution (blue region) is beyond the capabilities of modern manufacturing; the only hope for obtaining such resolution is to use computational techniques such as Fourier ptychography. (b) Affordable camera systems (solid marks) are lightweight but cannot resolve fine details over great distances as well as professional lenses (striped marks) which are heavy and expensive. We propose using a camera array with affordable lenses and active lighting to achieve and surpass the capabilities of professional lenses. Note: These plots only account for diffraction blur and do not consider other factors limiting resolution (see \tablename~\ref{tab:motivTab} for more details).}%
\label{fig:motivFig}%
\vspace{-\baselineskip}
\end{figure*}

New computational imaging techniques are being developed to improve resolution by capturing and processing a collection of images. 
Here, we focus on one such multi-image fusion technique designed specifically for the problem of recovering image details well below the diffraction limit.
It is convenient to perform multi-image fusion with a camera array, which can simultaneously acquire multiple images from different perspectives.
While camera arrays such as PiCam \cite{venkataraman2013picam} and Light\footnote{http://light.co} are becoming increasingly popular and have demonstrated resolution improvement, their applications are typically limited to improving resolution limits imposed by pixel sub-sampling.
They do not naturally extend to overcome the limits of diffraction blur.

Recent advances in ptychography have demonstrated that one can image beyond the diffraction limit of the objective lens in a microscope \cite{zheng2013wide}.
Ptychography typically captures multiple images using a programmable coherent illumination source, and combines these images using an appropriate phase retrieval algorithm. 
It is also possible to implement ptychography by keeping the illumination source fixed, and instead spatially shifting the camera aperture \cite{dong2014aperture}.
Under this interpretation, ptychography closely resembles the technique advocated in this paper to improve resolution via a ``coherent camera array''.
The goal of such a coherent array would be to recover both the amplitude and the phase of an optical field incident upon multiple spatially offset cameras, across the entire angular extent of the array. 
This matches the goal of creating a synthetic aperture via holography \cite{hillman2009high,martinez2008synthetic,massig2002digital}, but without directly measuring the phase of the incident field. 

In this paper, we explore the utility of ptychography-based methods for improving the imaging resolution in long-range imaging scenarios \cite{dong2014aperture}.
We analyze reconstruction performance under various scenarios to study the effects of noise, required degree of image overlap, and other important parameters and show that under practical imaging scenarios, coherent camera arrays have the potential to greatly improve imaging performance.
Further, we build a table-top prototype (shown in \figurename~\ref{fig:diffractionLimited}c) and show real results of several subjects, demonstrating up to a $7\times$ quantitative improvement in spatial resolution.
While confined to imaging within a lab, the prototype can be used to simulate many of the properties that we advocate for in a coherent camera array.
Finally, we demonstrate that multiplexed illumination can be used to build a coherent camera array capable of capturing scenes in a single snapshot.