\documentclass[journal,10pt]{IEEEtran}

\usepackage[pdftex]{graphicx}
   \graphicspath{{figures/}}
   \DeclareGraphicsExtensions{.pdf,.jpg,.png}
\usepackage{amsmath}
\usepackage{amssymb}
\usepackage{enumerate}
\usepackage{booktabs}
\usepackage{tabulary}
\usepackage[colorlinks=true,citecolor=black,urlcolor=blue,linkcolor=black]{hyperref}

\hyphenation{op-tical}



\newcommand{\Section}[1]{\vspace{-4pt}\section{#1}\vspace{-4pt}}
\newcommand{\Subsection}[1]{\vspace{-3pt}\subsection{#1}\vspace{-3pt}}
\newcommand{\Subsubsection}[1]{\vspace{-4pt}\subsubsection{#1}\vspace{-4pt}}
\DeclareMathOperator*{\argmin}{arg\,min}

\newenvironment{tight_itemize}{\begin{itemize} \itemsep
-3pt}{\end{itemize}}
\newenvironment{tight_enumerate}{\begin{enumerate} \itemsep
-3pt}{\end{enumerate}}

\linespread{0.99}



\begin{document}
%
\title{Toward Long Distance, Sub-diffraction Imaging Using Coherent Camera Arrays}
%
% author names and IEEE memberships
\author{Jason~Holloway*,~\IEEEmembership{Student Member,~IEEE,}
        M.~Salman~Asif,
				Manoj~Kumar~Sharma,
				Nathan~Matsuda,~\IEEEmembership{Student Member,~IEEE,}
				Roarke~Horstmeyer,
				Oliver~Cossairt,~\IEEEmembership{Member,~IEEE,}
				and~Ashok~Veeraraghavan,~\IEEEmembership{Member,~IEEE}% <-this % stops a space
\thanks{J.~Holloway, M.S.~Asif, and A.~Veeraraghavan are with the Dept. of Electrical and Computer Engineering, Rice University, Houston, TX, 77005 USA e-mail: jh25@rice.edu}% <-this % stops a space
\thanks{M.K.~Sharma, N.~Matsuda, and O.~Cossairt are with the Dept. of Electrical Engineering and Computer Science, Northwestern University, Evanston, IL, 60208.}% <-this % stops a space
\thanks{R.~Horstmeyer is with the Dept. of Electrical Engineering, California Institute of Technology, Pasadena, CA, 91125.}
}%

\IEEEpeerreviewmaketitle

\ifCLASSOPTIONcaptionsoff
  \newpage
\fi


\maketitle

%%%%%%% ABSTRACT
\input{01_abstract}

%%%%%%%% BODY TEXT
\Section{Introduction}
\label{sec:Intro}
\input{02_intro}

\Section{Resolution Limitations in Long Range Imaging}
\label{sec:ResLimits}
\input{03_resLimits}

\Section{Related Work}
\label{sec:Related}
\input{04_related}

\Section{Fourier Ptychography for Long Range Imaging}
\label{sec:fourierPtych}
\input{05_cost}

\Section{Algorithm for Image Recovery}
\label{sec:imRecovAlgo}
\input{05.1_imageRecovery}

\Section{Performance Analysis and Characterization}
\label{sec:simRecov}
\input{06_simExperiments}

\Section{Experimental Results}
\label{sec:realExp}
\input{07_realExp}

\Section{Barriers to Building a Real System}
\label{sec:prelimWork}
\input{08_nextSteps}
%
\Section{Conclusion}
\label{sec:conclusion}
\input{09_conclusion}

\vspace{-0.12in}
\section*{Acknowledgment}
\vspace{-0.06in}
This work was supported in part by NSF grants IIS-1116718, CCF-1117939, CCF-1527501, NSF CAREER grant IIS-1453192, ONR grant 1(GG010550)//N00014-14-1-0741, and a Northwestern University McCormick Catalyst grant. The authors would also like to thank Richard Baranuik and Aggelos Katsaggelos for their fruitful discussions and comments.

{\small
\bibliographystyle{ieee}
\bibliography{ptychBib}
}


% biography section
\begin{IEEEbiography}[{\includegraphics[width=1in,height=1.25in,clip,keepaspectratio]{Author_Holloway}}]{Jason Holloway}
is a Ph.D. candidate working under the guidance of Ashok Veeraraghavan in the Electrical and Computer Engineering department of Rice University. His research interests are in computational imaging \& photography and computer vision. He earned his B.S. degrees in Electrical and Computer Engineering and Physics from Clarkson University in 2010 and his M.S. in Electrical and Computer Engineering from Rice University in 2013.
\end{IEEEbiography}

\begin{IEEEbiography}[{\includegraphics[width=1in,height=1.25in,clip,keepaspectratio]{Author_Asif}}]{Salman Asif}
is a Postdoctoral Scholar in the Department of Electrical and Computer Engineering at Rice University. Dr. Asif received his B.Sc. degree in 2004 from the University of Engineering and Technology, Lahore, Pakistan, and an M.S.E.E degree in 2008 and a Ph.D. degree in 2013 from the Georgia Institute of Technology, Atlanta, Georgia. His research interests include compressive sensing, computational and medical imaging, and machine learning.
\end{IEEEbiography}
\vspace{-10ex}
\begin{IEEEbiography}[{\includegraphics[width=1in,height=1.25in,clip,keepaspectratio]{Author_Sharma}}]{Manoj K. Sharma}
is a Postdoctoral Scholar with the Department of Electrical and Computer Engineering at Northwestern University, USA. Dr. Sharma received his Ph.D. degree in 2013 from the Indian Institute of Technology, Delhi, India. He worked as a visiting researcher at Indian Institute of Space science and Technology (IIST) India. His research interests include image processing, image enhancement, phase retrieval,  compressive sensing, computational and high resolution imaging.
\end{IEEEbiography}
\vspace{-10ex}
\begin{IEEEbiography}[{\includegraphics[width=1in,height=1.25in,clip,keepaspectratio]{Author_Matsuda}}]{Nathan Matsuda}
is a Ph.D. candidate and NSF graduate research fellow at Northwestern University in Prof. Ollie Cossairt's computational photography lab. Nathan's work includes real-time 3D scanning in challenging environments, low-cost surface characterization for cultural heritage applications, and novel 3D displays for virtual reality systems. Prior to grad school, Nathan worked as a visual effects artist with over two dozen credits in film and television, including a primetime Emmy nomination for ABC's Once Upon A Time.
\end{IEEEbiography}
\enlargethispage{-5in}
\newpage
\begin{IEEEbiography}[{\includegraphics[width=1in,height=1.25in,clip,keepaspectratio]{Author_Horstmeyer}}]{Roarke Horstmeyer}
is currently an Einstein International Postdoctoral Fellow at Charit\`{e} Medical University in Berlin, Germany. He recently received his PhD from the Electrical Engineering department at Caltech, where he was a member of the Biophotonics Lab. Roarke received a Masters degree from the MIT Media Lab and a BS degree in Physics and Japanese from Duke University. His primary research interests include computational optics, wavefront shaping, and techniques to image deep into tissue.
\end{IEEEbiography}
\vspace{-10ex}
\begin{IEEEbiography}[{\includegraphics[width=1in,height=1.25in,clip,keepaspectratio]{Author_Cossairt}}]{Oliver Cossairt}
is an Assistant Professor at Northwestern University. Before joining Northwestern, he earned a Masters degree at the MIT Media Lab. He earned his Ph.D. from Columbia University, where he was awarded an NSF Graduate Research Fellowship. Prof. Cossairt is the director of the Computational Photography Laboratory at Northwestern University, and his research interests include optics/photonics, computer graphics, computer vision, and image processing. 
\end{IEEEbiography}
\vspace{-10ex}
\begin{IEEEbiography}[{\includegraphics[width=1in,height=1.25in,clip,keepaspectratio]{Author_Veeraraghavan}}]{Ashok Veeraraghavan}
is currently an Assistant Professor of Electrical and Computer Engineering at Rice University where he directs the Computational Imaging and Vision Lab. His research interests include computational imaging, computer vision and scaleable health.
He received his Bachelors in Electrical Engineering from the Indian Institute of Technology, Madras in 2002 and M.S and Ph.D. degrees from the Department of Electrical and Computer Engineering at the University of Maryland, College Park in 2004 and 2008 respectively.
\end{IEEEbiography}
\enlargethispage{-5in}

\end{document}