Accurate retrieval of optical phase requires redundant measurements.
If the final high-resolution image is comprised of $n$ pixels that contain both intensity and phase information, then it is clear that we must acquire at least $2n$ measurements of optical intensity from the image sensor.
However, it is not clear if additional data should be acquired (for example to improve tolerance to noise), how much this additional data might improve the quality of our reconstructions at different resolution scales, or how many images on average might be required to achieve a certain resolution goal.
We now explore these questions via simulation.

\Subsection{Fundamental Factors that Affect Recovery Performance}
There are two parameters that will influence the number of required images.
The first consideration is the desired resolution limit of our coherent camera array (i.e., its minimum resolvable feature), which is equivalent to specifying the synthetic aperture size.
The second consideration is the desired degree of data redundancy, which is equivalent to specifying the amount of overlap between adjacent images when viewed in the Fourier domain.
Computational cost is proportional to the number of acquired images, so the user will be eventually forced to trade off reconstruction quality for reconstruction time.

\Subsection{Experimental Design}
To explore these two parameters, we perform experimental simulations using a $512$ px $\times 512$ px resolution chart shown in \figurename~\ref{fig:FPsampling}(a).
This chart contains line pairs with varying widths from $20$ pixels down to $1$ pixel, corresponding to line pairs per pixel in the range $[0.025,0.5]$.
We assume each object is under coherent plane wave illumination from a distant source.
Diffraction blur is determined solely by the wavelength of the coherent source and the ratio between the focal length and aperture diameter (i.e. the $f/\#$). 
Recall that the radius of the diffraction spot size on the sensor is approximately $1.22\frac{\lambda f}{d}$.
We assume that the illumination wavelength is $550$ nm, the focal length of the lens is $800$ mm, and the aperture diameter is $18$ mm.
The resolution chart itself is $64$ mm $\times 64$ mm and is located $50$ meters away from the camera.
The aperture of the imaging system is scanned across the Fourier plane to generate each bandpassed optical field at the image plane.
A pictorial representation of the sampling pattern and resulting captured images is shown in \figurename~\ref{fig:FPsampling}(b)-(d).

For simplicity, we treat the target resolution chart as an amplitude object, which may be considered to be printed on a thin glass substrate or etched out of a thin opaque material.
We use the iterative algorithm described in Section~\ref{sec:imRecovAlgo} to recover the complex field of the resolution chart.
The algorithm terminates whenever the relative difference between iterations falls below $10^{-5}$ or after $1000$ iterations.
To quantify the reconstruction we compute the contrast for the horizontal and vertical bars belonging to each group.
Contrast, $C$, is defined as 
\begin{equation}
C = \frac{\overline{w} - \overline{b}}{\overline{w} + \overline{b}},
\label{eq:contrast}
\end{equation}
where $\bar{w}$ and $\bar{b}$ denote the average intensity of the white and black bars respectively. 
To aid our discussion, we define the limit of resolvability to be when the contrast of a group drops below $20\%$ (MTF$20$).
For the simulation experiments, we will assume the image sensor has a pixel width of $2~\mu$m.
It is important to note that in the simulation experiments we will only consider the effects of diffraction blur as the factor which limits resolution in long range images.

In our first simulation experiment, we capture a $21\times21$ grid of images with $61\%$ overlap between neighboring images.
Each captured image is $512$ px $\times512$ px, but high frequency information has been lost due to the bandpass filter.
Under this setup the Synthetic Aperture Ratio (SAR), given as the ratio between the synthetic aperture diameter and lens aperture diameter, is $8.8$.
\figurename~\ref{fig:simResChart}(b) shows the center observed image of the resolution target, in which features smaller than $12$ pixels are lost, due to the low-pass filtering of the aperture.
This corresponds to the best resolution that can be achieved in coherent illumination without using ptychography to recover additional frequency information.
If we use ptychography and phase retrieval, the resolution increases significantly; features as small as $2$ pixels can be recovered as shown in \figurename~\ref{fig:simResChart}(d).
Plots comparing the contrast of the image intensity for the center observation (dashed blue line) and the recovered image (solid purple line) are provided in \figurename~\ref{fig:simResChart}(c).
The recovered Fourier magnitude and zoomed in detail views of groups 2, 5, and 6 are shown in \figurename~\ref{fig:simResChart}(e) and (f) respectively, showing faithful recovery of otherwise unresolved features.
\begin{figure}[t!]%
\centering
\includegraphics[width=\linewidth]{simResults_resChart}
\caption{\textbf{Recovering high frequency information using a Fourier ptychography.}
(a) We simulate imaging a $64 \times 64$ mm resolution target $50$ meters away using sensor with a pixel pitch of $2~\mu$m. The width of a bar in group $20$ is $2.5$ mm.
(b) The target is observed using a lens with a focal length of $800$ mm and an aperture of $18$ mm. The aperture is scanned over a $21\times21$ grid ($61\%$ overlap) creating a synthetic aperture of $160$ mm. The output of phase retrieval is a high resolution Fourier representation of the target.
The recovered image is shown in (d) and the recovered Fourier magnitude (log scale) is shown in (e).
The plot in (c) shows the contrast of the groups in the intensity images of the recovered resolution chart (purple solid line) and the observed center image (blue dashed line).
Whereas the central image can only resolve elements which have a width of $12$ pixels before contrast drops below $20\%$, using Fourier ptychography we are able to recover features which are only $2$ pixels wide.
Detail images of groups 2, 5, and 6, for the ground truth, observed, and recovered images are shown in (f).}%
\label{fig:simResChart}%
\end{figure}

Both the cost of assembling the camera array and the computational complexity of our reconstruction algorithm is $O(N^2)$.
Therefore, minimizing the number of captured images necessary to recover each high resolution image is an important design criteria.
There are two degrees of freedom to explore to help reduce the amount of ptychographic data acquired: changing the amount of overlap between neighboring images and changing the SAR.

\Subsection{Effect of Overlap}
Intuitively, increasing the amount of overlap should improve reconstruction performance. 
Redundant measurements help to constrain the reconstruction and provide some robustness to noise.
In \figurename~\ref{fig:varySpacing_resChart}, we show the effects of increasing the amount of overlap from $0\%$ (no overlap) to $77\%$, a large degree of overlap. 
The SAR is held constant at $10$ for each overlap value, resulting in a $9\times9$ sampling grid for $0\%$ overlap and a much larger $41\times41$ grid for $77\%$ overlap.
As seen in \figurename~\ref{fig:varySpacing_resChart}, the root-mean-squared error (RMSE) between the ground truth and recovered intensities decreases as the amount of overlap increases with a precipitous drop when the amount of overlap is greater than $\sim\hspace{-1ex} 50\%$. 
This watershed demarcates the boundary of recovering the high resolution image.

\begin{figure}[t!]%
\centering
\includegraphics[width=\columnwidth]{varySpacing_resChart.pdf}%
\caption{\textbf{Effect of varying overlap between adjacent images.} Holding the synthetic aperture size constant, we vary the amount of overlap between adjacent images. As the amount of overlap increases, reconstruction quality improves. When the amount of overlap is less than $50\%$, we are unable to faithfully recover the high resolution image, as shown in the RMSE plot in the top left. The contrast plots of the intensity images for four selected overlap amounts ($0\%,41\%,50\%,$ and $75\%$) shows that insufficient overlap drastically reduces the resolution of the system. Recovered images show the increase in image reconstruction quality as the amount of overlap increases.}%
\label{fig:varySpacing_resChart}%
\vspace{-\baselineskip}
\end{figure}

We can see the effect of varying the amount of overlap in the contrast plot and the recovered images shown in \figurename~\ref{fig:varySpacing_resChart}.
Images captured with overlaps of $0\%$ and $41\%$ have large RMSE, fail to recover even low resolution features (even though the SAR is constant), and the resulting images are of low quality.
Conversely, data captured with overlaps of $50\%$ and $75\%$ are able to reconstruct small features with low RMSE.
Reconstruction quality also increases as the amount of overlap increases.
Thus $50\%$ is a lower bound on the amount of overlap required to run phase retrieval for an ideal imaging system~\cite{schniter2015compressive,heinosaari2013quantum}. Additional overlap may be necessary for a real camera system.
This observation coincides with previously reported overlap factors of $60\%$ for good reconstruction~\cite{bunk2008influence}.

\Subsection{Effect of Synthetic Aperture Ratio (SAR)}
Alternatively, the number of images may be reduced by decreasing the synthetic aperture ratio.
For a given overlap, smaller SAR values will yield less improvement in resolution than larger SAR values (and a corresponding increase in the number of images).
To demonstrate this, we use the same camera parameters as the previous experiment and fix overlap at $61\%$ while varying the SAR.
\figurename~\ref{fig:varyN_resChart} shows that larger apertures can resolve smaller features, which can be clearly seen in the contrast plot.
Without using ptychography, features less than $12$ pixels wide ($0.04$ line pairs/pixel, $25~\mu$m at the image plane) cannot be resolved.
Resolution steadily improves as the SAR increases to $5.64$ before holding steady until an SAR of $11.8$.
As the SAR increases, image contrast also increases, which can be seen in the detail images shown in \figurename~\ref{fig:varyN_resChart}.
Of course, increasing the SAR requires additional images; an SAR of $1.77$ requires a $3\times3$ sampling grid, while SARs of $3.32$, $4.09$, $5.64$, and $11.8$ require $7\times7$, $9\times9$, $13\times13$, and $29\times29$ grids respectively.
While, in theory, coherent camera arrays can improve resolution down to the wavelength of the illumination source, an order of magnitude in SAR requires a quadratic increase in the number of recorded images.
Thus, the SAR should be set to the smallest size which meets the resolution requirement for a given application.

\begin{figure}[t!]%
\centering
\includegraphics[width=\columnwidth]{varyN_resChart.pdf}%
\caption{\textbf{Varying synthetic aperture ratio (SAR).} For a fixed overlap of $61\%$, we vary the size of the synthetic aperture by adding cameras to array. As seen in the contrast plot of image intensities, as well as the reconstructed intensities below, the resolution of the recovered images increase as the SAR increases from $1$ (the observed center image) to $11.8$. We are able to recover group $2$ with an SAR of $5.64$, which requires $169$ images. }%
\label{fig:varyN_resChart}%
\vspace{-\baselineskip}
\end{figure}

\Subsection{Effect of Noise on Image Recovery}
The phase retrieval problem in \eqref{eq:problem1} is non-convex and thus prone to getting stuck in local minima when using gradient descent procedures such as alternating minimization.
Nonetheless, we observe that in practice our algorithm converges well for a variety of scenes and noise levels.
We repeat the varying SAR experiment using the canonical Lena image, and with varying levels of additive white Gaussian noise in the 
observed images.
We test signal-to-noise ratios (SNR) of $10$, $20$, and $30$ dB, and present the results in \figurename~\ref{fig:varyN_lena}.
Noise is added such that each captured image has the desired SNR.
As shown in the RMSE plot, we are able to recover the high resolution image even with input images containing significant noise.
The observed center images (SAR=$1$) reflect this noise, as well as diffraction blur, and thus exhibit a high RMS error.
As the SAR increases, the resolution improves, the noise is suppressed, and the RMSE decreases (thanks to redundancy in the measurements).
All remaining simulation experiments are conducted with an input SNR of $30$ dB.

\Subsection{Comparison with Expensive Large Aperture Lenses}
\label{sec:experimentalSim}
Next, we compare simulated images from an expensive and bulky lens, with simulated images from our coherent camera array. 
For these experiments we assume each system exhibits a fixed focal length of $1200$ mm.
In the early 1990s, Canon briefly made a $1200$ mm lens with a $215$ mm diameter aperture\footnote{Canon EF $1200$mm $f/5.6$L USM} which retailed for nearly \$$150{,}000$ US dollars (inflation adjusted to 2015) and weighed nearly $17$ kilograms. 
At $30$ meters, its minimum resolvable feature size is $190 \mu$m at the object plane (Nyquist-Shannon limit).
This diffraction spot size is just small enough to recover the pertinent features of a fingerprint where the average ridge width is on the order of $400~\mu$m \cite{orczyk2011fingerprint}.

\begin{figure}[t!]%
\centering
\includegraphics[width=\columnwidth]{varyN_lena.pdf}%
\caption{\textbf{Noise robustness of the coherent camera array.} We repeat the experiment in \figurename~\ref{fig:varyN_resChart} with the Lena image (assuming the scene is $650$ meters away), while varying the amount of added Gaussian noise. The signal-to-noise ratio (SNR) of the input images is set to be $10$, $20$, and $30$ dB. As shown in the RMSE plot, we are able to recover the high resolution image even with noisy input images. The observed center images (SAR=$1$) are blurry and noisy, with a high RMS error. As the SAR increases, the resolution improves, the noise is suppressed, and RMSE decreases. A cropped portion of select images are shown to highlight the performance of our approach.  Note: all other simulation experiments are conducted with an input SNR of $30$ dB.}%
\label{fig:varyN_lena}%
\vspace{-\baselineskip}
\end{figure}

Instead of using such a large and expensive lens, one could alternatively consider imaging with a much smaller lens with $1200$ mm focal length and a $75$ mm diameter which may cost on the order of hundreds of dollars to manufacture.
Such a lens would have a diffraction spot size of $21~\mu$m on the image sensor, and a minimum resolvable feature size of $536~\mu$m at $30$ meters.
The poor performance of this lens precludes identifying fingerprints directly from its raw images.
However, incorporating such a lens into a coherent camera array, capturing multiple images, and reconstructing with ptychographic phase retrieval can offer a final diffraction-limited resolution that surpasses the large Canon lens.

We simulate imaging a fingerprint at $30$ meters with both of the above approaches in \figurename~\ref{fig:simResults_fingerprint}
We use a fingerprint from the $2004$ Fingerprint Verification Challenge \cite{maltoni2009handbook} as ground truth, where each pixel corresponds to $51~\mu$m at the object plane, and the pixel size on the camera sensor is $2~\mu$m (as before).
Using a $1200$ mm focal length, $75$ mm aperture lens with incoherent illumination yields images in which blur has degraded the image quality so that they are of no practical use (\figurename~\ref{fig:simResults_fingerprint}(a)).
However, if the same lens is used to simulate an aperture diameter of $300$ mm in a coherent camera array ($61\%$ overlap, $9\times9$ images, SAR=$4$), the diffraction spot size reduces to $130~\mu$m, which is sufficient to identify the print, as shown in \figurename~\ref{fig:simResults_fingerprint}(b).
The Canon lens with a $215$ mm aperture could be used to achieve comparable quality, as in \figurename~\ref{fig:simResults_fingerprint}(c).
Detail images in \figurename~\ref{fig:simResults_fingerprint}(d) highlight the image quality of the three imaging systems.

A similar experiment is conducted in \figurename~\ref{fig:simResults_face} with the subject being a human face $1000$ meters away.
In this experiment, a synthetic aperture of $300$ mm, corresponds to a diffraction spot size of $4.4$ mm on the face.
The $75$ mm lens has a diffraction spot size of $180$ mm and is again unable to resolve the scene with incoherent illumination but fine details are recovered using ptychography, \figurename~\ref{fig:simResults_face}(a) and (b) respectively.
The Canon lens (\figurename~\ref{fig:simResults_fingerprint}(c)), yields a blurry image that is inferior to the image acquired under active illumination.
Detailed crops of one of the eyes acquired using the three imaging setups are shown in \figurename~\ref{fig:simResults_face}(d).

While these simulations are limited in scope (i.e. we assume that diffraction is the only factor limiting resolution and neglect phase), the results suggest that implementing Fourier ptychography in a camera array offers a practical means to acquire high resolution images while remaining cost competitive with existing lenses.
Moreover, such computational approaches appear as the only means available to surpass the resolution limits imposed by current manufacturing technology.
%
\begin{figure}[t!]%
\centering
\includegraphics[width=\linewidth]{simFingerprint}
\caption{\textbf{Simulation of imaging a fingerprint at $30$ meters.} We simulate image capture with an inexpensive $1200$ mm lens, $75$ mm aperture diameter ($f/16$), and a pixel pitch of $2~\mu$m. (a) Using passive illumination the resulting diffraction blur ($530~\mu$m) removes details necessary to identify the fingerprint. (b) Imaging with a coherent camera array ($61\%$ overlap) that captures $81$ images to create a synthetic aperture of $300$ mm reduces diffraction blur to $130~\mu$m, and leads to faithful recovery of minutiae. (c) Using a $\$150{,}000$ lens with a $215$ mm aperture diameter ($f/5.6$), the diffraction blur reduces to $190~\mu$m, which is roughly comparable to our final reconstruction in (b). (d) Detail views of the three imaging systems. 
In this simulation diffraction is the only source of blur, other factors in \tablename~\ref{tab:motivTab} are not considered.}%
\label{fig:simResults_fingerprint}%
\vspace{-\baselineskip}
\end{figure}