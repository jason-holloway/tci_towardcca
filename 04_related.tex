Computationally improving camera resolution is a longstanding goal in imaging sciences. Here, we summarize the prior work most relevant to our coherent camera array setup. 

\begin{figure}[t!]%
\centering
\includegraphics[width=\linewidth]{incoherent_superresolution}%
\caption{\textbf{Comparison with multi-image super-resolution under large diffraction blur}
Left: When diffraction blur is not much larger than a pixel, conventional multi-image super-resolution methods achieve a $2\times$ improvement in resolution while the proposed method offers a $4\times$ improvement in this example (greater resolution improvements are shown in Section~\ref{sec:simRecov} and Section~\ref{sec:realExp}). Right: Under extreme diffraction blur, conventional methods break down and offer no improvement. Only with the proposed method can distinct image features be recovered. The diffraction spot size for each scenario is represented by the pink circle on the sensor grid; the high resolution ground truth image is presented in \figurename~\ref{fig:FPsampling}.
}%
\label{fig:passive_vs_active}%
\vspace{-\baselineskip}
\end{figure}

\textbf{Multi-Image Super-Resolution:}
Multi-image super-resolution has received much attention over the past $20$ years (see \cite{nasrollahi2014super} for a comprehensive survey of the field) and continues to be an important area of study in modern camera arrays \cite{carles2014super,venkataraman2013picam}.
However, the overall improvement in resolution has been modest; practical implementations achieve no more than a $2\times$ improvement \cite{baker2002limits,lin2004fundamental,milanfar2010super}.
Exemplar based efforts to improve the super-resolution factor beyond $2$ use large dictionaries to hallucinate information \cite{freeman2002example,glasner2009super} and offer no guarantees on fidelity.
Even these techniques are typically limited to producing a $4\times$ improvement in resolution \cite{glasner2009super}.
In this paper, we present a technique that can, in theory, improve resolution all the way to the diffraction limit of light (e.g. below a micron for visible light).
Our experimental results clearly indicate significant resolution enhancements over and above those achieved by traditional multi-image techniques, particularly in the regime of large diffraction blur.

In \figurename~\ref{fig:passive_vs_active}, we simulate super-resolution gains using a conventional imaging system and our proposed imaging system.
A $512 \times 512$ pixel resolution target (shown in full in \figurename~\ref{fig:FPsampling}) is imaged onto a $64\times 64$ pixel sensor with a pixel pitch of $2~\mu$m, for an $8\times$ loss in resolution.
In the left column of \figurename~\ref{fig:passive_vs_active}, a lens with a large aperture ($f/5.6$) produces a diffraction spot size of $3.7~\mu$m.
Due to the pixel sampling, the smallest discernible features belong to group $12$.
Common multi-image super-resolution techniques such as Papoulis-Gerchberg super-resolution \cite{gerchberg1974super,papoulis1975new}, robust super-resolution \cite{zomet2001robust}, and normalized convolution \cite{pham2006robust} yield comparable $2\times$ improvement, resolving features in group $6$.
These algorithms are implemented using code made available from \cite{vandewalle2006frequency}.
Active super-resolution provides even greater resolution, resolving features down to group $3$, for a four-fold improvement (purple box).
Full details regarding the proposed image acquisition and recovery process will be provided in subsequent sections.

In the presence of large diffraction blur, conventional super-resolution methods fail to improve spatial resolution to any appreciable degree.
\figurename~\ref{fig:passive_vs_active} (right) illustrates the same imaging scenario as before, but using a lens with smaller diameter.
Using a $f/32$ lens results in diffraction blur of $21.5~\mu$m.
Conventional methods fail to recover any spatial information, whereas our proposed active imaging system can resolve features in group $12$ and separates the smaller groups into distinct features.
In this paper we will restrict our scope to the regime of large diffraction blur, where conventional super-resolution methods are ill-suited for improving resolution; however, the proposed solution may also improve spatial resolution for imaging systems with small diffraction blur.


\textbf{Camera Arrays:}
Camera arrays comprised of numerous ($>\hspace{-1ex}10$) cameras, have been proposed as an efficient method of achieving a depth-of-field that is much narrower than that of a single camera in the array. 
Large arrays, such as the Stanford camera array \cite{wilburn2005high}, demonstrated the ability to computationally refocus. 
Camera arrays are also well-suited to astronomical imaging. 
The CHARA array \cite{CHARA} is an collection of telescopes ($1$ meter diameter) on Mount Wilson in California which are jointly calibrated to resolve small astronomical features (angular resolution of $200$ microarcseconds), using a synthetic aperture with a diameter of $330$ meters.

Despite the advantages provided using a camera array, diffraction blur limits resolution in long distance imaging.
For scenes lit with incoherent illumination, such as sunlight, multi-image super-resolution techniques can further increase spatial resolution by a factor of two.
Even with this modest improvement in resolution, the remaining blur due to diffraction may still preclude recovering fine details.
In this paper we propose using active illumination to recover an image with spatial resolution many times greater than the limit imposed by optical diffraction.

\textbf{Fourier Ptychography}
Fourier ptychography (FP) has permitted numerous advances in microscopy, including
wide-field high-resolution imaging~\cite{zheng2013wide}, removing optical~\cite{horstmeyer2014overlapped} and pupil~\cite{ou2014embedded} aberrations, digital refocusing~\cite{dong2014aperture}, and most recently $3$D imaging of thick samples~\cite{li2015separation,tian20153d}.
While most FP microscopy methods are transmissive, there is a nascent interest in reflective mode ptychography~\cite{seaberg2014tabletop,harada2013phase}, though we know of no FP work for far field imaging.
FP is, in effect, a synthetic aperture technique~\cite{hillman2009high,martinez2008synthetic,massig2002digital,mico2006synthetic} that does not require phase measurements.
In FP microscopy, the imaging lens is often fixed while an array of light sources is used to shift different regions of the image spectrum across a fixed aperture of limited size.
Instead of advancing the light in a raster pattern, the illumination sources may be multiplexed to increase SNR and reduce acquisition time~\cite{dong2014spectral,tian2014multiplexed}.
Refinements in the sampling pattern can also reduce acquisition time~\cite{guo2015optimization}.
Initial work in macroscopic imaging~\cite{dong2014aperture} suggests the potential benefits of Fourier ptychography in improving spatial resolution of stand-off scenery.
In this paper we extend upon this work and characterize how the amount of overlap, synthetic aperture size, and image noise affects reconstruction quality and resolution improvement.
Furthermore, we describe early efforts towards realizing a physical system to beat diffraction in long range snapshot imaging systems.

\textbf{Phase Retrieval Algorithms:}
Phase retrieval is a required component of ptychographic image reconstruction.
Image sensors record the magnitude of a complex field, either in the Fourier domain for traditional ptychography or the spatial domain in Fourier ptychography.
Numerous algorithms for retrieving the missing phase information have been proposed. One of the simplest techniques is based upon alternating projections~\cite{gerchberg1972practical,fienup1982phase}.
Many extensions of this simple starting point exist for both standard phase retrieval using a single image~\cite{elser2003phase,fienup1986phase}, as well as ptychographic phase retrieval using multiple images~\cite{horstmeyer2015convex,maiden2009improved}.

\textbf{Alternative methods to improve resolution:}
The primary technique to improve image resolution in long-distance imaging is to simply build a larger telescope.
On the scale of portable imaging systems, this typically means removing large glass optical elements in favor of catadioptric lens designs, where a mirror reflects light through a much smaller lens.
Other techniques for improving resolution include lucky imaging to mitigate atmospheric turbulence~\cite{fried1978probability,joshi2010seeing,zhang2011efficient} in terrestrial imaging.
In astronomical imaging, motion tracking~\cite{dantowitz2000ground,mcclure1989image} and speckle imaging~\cite{labeyrie1970attainment,knox1974recovery,lohmann1983speckle} are common methods to improve image resolution.
These latter approaches tackle external factors limiting resolution, but do not address the effects of diffraction on spatial resolution.
