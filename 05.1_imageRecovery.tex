\begin{figure}[t!]%
\includegraphics[width=\linewidth]{AM_blockDiagram}
\caption{\textbf{Block diagram of the image recovery algorithm.} Constraints on the image domain magnitude and Fourier domain support are enforced in an alternating manner until convergence or a maximum iteration limit is met.}%
\label{fig:AM_blocks}%
\vspace{-\baselineskip}
\end{figure}

Using our coherent camera array measurements in \eqref{image}, our goal is to recover the complex-valued, high-resolution field $\widehat \psi(x',y')$.
Let us denote the image measured with camera at location $(c_{x'_i},c_{y'_i})$ as
\begin{equation}
I_i = |\psi_i|^2\equiv|\mathcal{F}\mathcal{R}_{\Omega_i} \widehat \psi|^2,
\end{equation}
where $\psi_i=\mathcal{F}\mathcal{R}_{\Omega_i} \widehat \psi$ denotes the complex-valued, bandlimited field whose intensity is measured at the image sensor, $\widehat \psi$ denotes the complex-valued field at the Fourier plane (i.e. the aperture plane), and $\mathcal{R}_{\Omega_i} \widehat \psi$ denotes an aperture operator that sets all the entries of $\widehat \psi(x',y')$ outside the set $\Omega_i = \{(x,y): |x-c_{x'_i}|^2 + |y-c_{y'_i}|^2 \le d/2\}$ to zero. 

To recover the high-resolution $\widehat\psi$ from a sequence of $N$ low-resolution, intensity measurements, $\{I_i\}_{i=1}^N$, we use an alternating minimization-based phase retrieval problem that is depicted in \figurename~\ref{fig:AM_blocks}.
The recovery algorithm is based on the error-reduction phase retrieval algorithm proposed by Gerchberg and Saxton~\cite{gerchberg1972practical}, which continues to form the basis of many modern FP algorithms~\cite{tian20153d}.
We seek to solve the following problem
\begin{equation}
\widehat\psi^* = \underset{\widehat\psi}{\operatorname{arg~min}} \sum_i{\left\|\psi_i - \mathcal{FR}_{\Omega_i} \widehat\psi\right\|_2}\;\; \textnormal{s.t.}\;\;|\psi_i|^2 = I_i,
\label{eq:problem1}
\end{equation}
by alternatively constraining the support of $\widehat\psi$ and the squared magnitudes of $\psi$.
We set the initial estimate of $\widehat\psi^0$ to be a scaled version of the Fourier transform of the mean of the low resolution images.
At every iteration ($k$), we perform the following three steps: 
\begin{enumerate}[\bf 1.]
	\item  Compute complex-valued images at the sensor plane using the existing estimate of the field at the Fourier plane, $\widehat \psi^k$:
	$$\psi_i^k = \mathcal{FR}_{\Omega_i} \widehat \psi^k \quad \text{for all } i.$$
	\item  Replace the magnitudes of $\psi_i^k$ with the magnitude of the corresponding observed images $I_i$: 
	$$ \psi_i^k \gets \sqrt{\frac{I_i}{|\psi_i^k|^2}}\; \psi_i^k \quad \text{for all } i.$$
	\item  Update the estimate of $\widehat \psi$ by solving the following regularized, least-squares problem: 
	\begin{equation}
	\widehat \psi^{k+1} \gets \underset{\widehat \psi}{\textnormal{minimize}}\; \sum_i \left\|\psi_i^k - \mathcal{FR}_{\Omega_i} \widehat \psi\right\|_2^2+\tau\|\widehat \psi\|_2^2,
	\end{equation}
	where $\tau>0$ is an appropriately chosen regularization parameter.
	Tikhonov regularization is used for numerical stability during reconstruction as in~\cite{tian20153d}.
	This problem has a closed form solution, which can be efficiently computed using fast Fourier transforms. 
\end{enumerate}