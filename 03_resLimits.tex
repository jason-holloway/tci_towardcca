%
\begin{table*}[t]%
\centering
\caption{Intrinsic and extrinsic factors  which limit resolution in long range imaging}
\label{tab:motivTab}
\input{figures/motivTable}
\vspace{-\baselineskip}
\end{table*}
%
Image resolution at long ranges is limited due to several inter-related factors as seen in \tablename~\ref{tab:motivTab}.
We classify these factors as being intrinsic to the imaging system (e.g. diffraction) and extrinsic to the imaging system (e.g. turbulence).

\textbf{Pixel Size:} 
A number of prior works developed techniques to overcome pixel-limited resolutions by capturing a sequence of images with sub-pixel translations \cite{borman1998spatial}. 
Some translation between the camera and scene is required between each exposure.
This is achieved using either small sensor movements, lens defocus, or movements of the entire imaging system \cite{baker2003shape}.
A sequence of sub-pixel shifted images may be acquired in a single snapshot by using a camera array.
However, current sensor pixels are approaching the diffraction limit of visible light ($1.2~\mu$m pixels are commercially widespread), and thus pixel sampling limits are not as critical a limitation to many current and future imaging platforms as in the past.


\textbf{Diffraction:}
Perhaps the most challenging impediment to imaging high resolution at a large distance is optical diffraction caused by the finite aperture size of the primary imaging lens.
While a wider lens will lead to a sharper point-spread function for a fixed focal length, it will also create proportionally larger aberrations. 
Additional optical elements may be added to the lens to correct these aberrations, but these elements will increase the size, weight and complexity of the imaging system \cite{lohmann1989scaling}. 
As noted in the introduction, the main focus of this work is to address this diffraction-based resolution limit with a joint optical capture and post-processing strategy. We use a small, simple lens system to capture multiple intensity images from different positions.
In our initial experiments our prototype uses a single camera on a translation stage that captures images sequentially (in approximately $90$ minutes).
The next phase of our research will focus on design, assembly, and calibration of a snapshot-capable camera array system.
We fuse captured images together using a ptychographic phase retrieval algorithm.
The result is a high resolution complex reconstruction that appears to have passed through a lens whose aperture size is equal to the synthetic aperture created by the array of translated camera positions.

\textbf{Additional causes of resolution loss:} We do not explicitly address the four remaining causes of resolution loss in this work.
We now mention their impact both for completeness and to highlight additional alternative benefits of the ptychographic technique.
First, intrinsic lens aberrations limit the resolution of every camera.
Second, turbulence is a large source of image degradation, especially when long exposure times are required.
Finally, noise and possible motion during the camera integration time leads to additional resolution loss. 

While not examined here, ptychography captures redundant image data that may be used to simultaneously estimate and remove microscope \cite{ou2014embedded} or camera \cite{horstmeyer2014overlapped} aberrations.
Redundant data is also helpful to address noise limits.
Furthermore, while yet to be demonstrated (to the best of our knowledge), is not hard to imagine that similar computational techniques may help estimate and remove external aberrations caused by turbulence, especially as our procedure simultaneously recovers the incident field's phase.
Ptychographic techniques thus appear ideally suited for overcoming the long-distance image resolution challenges summarized in \tablename~\ref{tab:motivTab}. 
In this paper, we address a limited problem statement; namely, can a ptychographic framework be extended to achieve sub-diffraction resolution at large standoff distances?