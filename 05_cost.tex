Our goal in this paper is to capture high resolution images of objects that are a considerable distance away from the camera, e.g. resolve a face $1000$ meters away.
Passive camera systems use available (incoherent) illumination sources such as the sun and suffer from significant diffraction blur.
Existing super resolution methods may be able to effectively reduce the amount of blur by a factor of two, which may be insufficient to resolve enough detail to recover the object of interest.
In order to resolve even smaller features, we assume that we have a coherent or partially coherent illumination source; that is, we have active illumination.

Like other multi-image super-resolution methods \cite{baker2002limits,lin2004fundamental} we will capture a series of low resolution images which we will then use to recover a high resolution image.
We take multiple images using a coherent illumination source where each image is from a different (known) position in the $XY$ plane.
For simplicity we assume the camera positions coincide with a regular grid, though this is not necessary \cite{guo2015optimization}.
It should be noted that we would obtain the same result by leaving the camera stationary and moving the illumination source, a common practice in microscopy~\cite{dong2014spectral,horstmeyer2015convex,ou2014embedded,tian2014multiplexed,tian20153d}.

\Subsection{Image Formation Model}
We first assume a single fixed illumination source\footnote{In Section~\ref{sec:prelimWork} we consider multiple illumination sources}, which can either be co-located with or external to our imaging system.
The source should be quasi-monochromatic, with center wavelength $\lambda$.
For color imaging, multiple quasi-monochromatic sources (i.e., laser diodes) are effective~\cite{zheng2013wide}.
The source emits a field that is spatially coherent across the plane which contains our object of interest, $P(x,y)$.
We assume this plane occupies some or all of the imaging system field of view. 

The illumination field, $u(x,y)$, will interact with the object, and a portion of this field will reflect off the object towards our imaging system.
For our initial experiment, we assume the object is thin and may be described by the $2$D complex reflectivity function $o(x,y)$.
While we discuss $2$D surfaces, extension to surface reflectance from $3$D objects follows from this analysis, see~\cite{seaberg2014tabletop,tian20153d,li2015separation} for examples of $3$D FP microscopy.
Under the thin object approximation, the field emerging from the object is given by the product $\psi(x,y)=u(x,y)o(x,y)$.
This then propagates a large distance $z$ to the far field, where our imaging system occupies the plane $S(x',y')$. 

Under the Fraunhofer approximation, the field at $S(x',y')$ is connected to the field at the object plane by a Fourier transform~\cite{born2000principles}:
\begin{equation}
\widehat{\psi}(x',y') = \frac{e^{jkz}e^{\frac{jk}{2z}(x'^2+y'^2)}}{j\lambda z} \mathcal{F}_{1/\lambda z} \left[\psi(x,y) \right]
\label{ft}
\end{equation}
where $k=2\pi/\lambda$ is the wavenumber and $\mathcal{F}_{1/\lambda z}$ denotes a two dimensional Fourier transform  scaled by $1/\lambda z$.
For the remainder of this manuscript, we will drop multiplicative phase factors and coordinate scaling from our simple model.
We note that the following analysis also applies under the Fresnel approximation (i.e., in the near field of the object), as supported by recent investigations in X-ray ptychography~\cite{edo2013sampling,thibault2008high}.

The far field pattern, which is effectively the Fourier transform of the object field, is intercepted by the aperture of our camera.
We describe our limited camera aperture using the function $A(x'-c_{x'},y'-c_{y'})$, which is centered at coordinate $(c_{x'},c_{y'})$ in the plane $S(x',y')$ and passes light with unity transmittance within a finite diameter $d$ and completely blocks light outside (i.e., it is a ``circ" function). 

The optical field immediately after the aperture is given by the product $\widehat{\psi}(x',y') A(x'-c_{x'},y'-c_{y'})$.
This bandlimited field then propagates to the image sensor plane.
Again neglecting constant multiplicative and scaling factors, we may also represent this final propagation using a Fourier transform.
Since the camera sensor only detects optical intensity, the image measured by the camera is 
\begin{equation}
I(x,y,c_{x'},c_{y'})\propto \left|\mathcal{F}\left[ \widehat{\psi}(x',y') A(x'-c_{x'},y'-c_{y'}) \right]\right|^{2}.
\label{image}
\end{equation}
In a single image, the lowpass nature of the aperture results in a reduced resolution image.
For an aperture of diameter $d$ and focal length $f$, diffraction limits the smallest resolvable feature within one image to be approximately $1.22\lambda f/d$. 

\begin{figure}[t!]%
\centering
\includegraphics[width=\columnwidth]{ptychSampling}
\caption{\textbf{Sampling the Fourier domain at the aperture plane.} For a given high resolution target (a), the corresponding Fourier transform is formed at the aperture plane of the camera (b). The lens aperture acts as a bandpass filter, only allowing a subset of the Fourier information to pass to the sensor. A larger aperture may be synthesized by scanning the aperture over the Fourier domain and recording multiple images. (c) The full sampling mosaic acquired by scanning the aperture. The dynamic range has been compressed by scaling the image intensities on a log scale. (d) Larger detail images shown of four camera positions, including the center. Image intensities have been scaled linearly, note that only high frequency edge information is present in the three extreme aperture locations. Please view digitally to see details.}%
\label{fig:FPsampling}%
\vspace{-\baselineskip}
\end{figure}

\Subsection{Fourier Ptychography to Improve Resolution}
Ptychography presents one strategy to overcome the diffraction limit by capturing multiple images and synthetically increasing the effective aperture size.
The series of captured images is used to recover the high resolution complex field in the aperture plane and subsequently a high resolution image.

To achieve this, we re-center the camera at multiple locations, $\left(c_{x'_i},c_{y'_i}\right)$, and capture one image at the $i$th camera location, for $i=1,\ldots,N$.
This transforms \eqref{image} into a four-dimensional discrete data matrix.
The $N$ images can be captured in a number of ways, one can: physically translate the camera to $N$ positions, construct a camera array with $N$ cameras to simultaneously capture images, fix the camera position and use a translating light source, or use arbitrary combinations of any of these techniques.

If we select our aperture centers such that they are separated by the diameter $d$ across a rectilinear grid, then we have approximately measured values from the object spectrum across an aperture that is $\sqrt{N}$ times larger than what is obtained by a single image.
Thus, it appears that such a strategy, capturing $N$ images of a coherently illuminated object in the far field, may be combined together to improve the diffraction-limited image resolution\footnote{It should be noted that it is not possible to improve resolution below the diffraction limit of the light source $1.22\lambda/2$, though practical considerations will prevent this from being a concern in macroscopic FP} to $1.22\lambda f/\sqrt{N}d$.

However, since our detector cannot measure phase, this sampling strategy is not effective as-is.
Instead, it is necessary to ensure the aperture centers overlap by a certain amount (i.e., adjacent image captures are separated by a distance $\delta<d$ along both $x'$ and $y'$).
This yields a certain degree of redundancy within the captured data, which a ptychographic post-processing algorithm may utilize to simultaneously determine the phase of the field at plane $S(x',y')$.
Typically, we select $\delta\sim0.25d$.
See \figurename~\ref{fig:FPsampling} for an overview of the sampling strategy and example images recorded with the aperture acting as a bandpass filter of the Fourier transform $\widehat{\psi}(x',y')$.
Next, we detail a suitable post-processing strategy that converts the data matrix $I\left(x,y,c_{x'_i},c_{y'_i}\right)$ into a high-resolution complex object reconstruction.