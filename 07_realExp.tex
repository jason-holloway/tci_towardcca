In this section, we experimentally verify our simulations of the coherent camera array. 
For simplicity, we use a transmissive setup similar to existing ptychography methods applied in microscopy.
While this configuration is not practical for long distance imaging, we present the results as a proof-of-concept that we hope will directly extend to a reflection geometry.
In Section~\ref{sec:prelimWork} we discuss the practical limitations of implementing a long-range ptychography imaging system.

\Subsection{Experimental Prototype}
Our imaging system consists of a Blackfly machine vision camera manufactured by Point Grey (BFLY-PGE-50A2M-CS), coupled with a $75$ mm Fujinon lens (HF75SA-1) that is mounted on a motorized $2$D stage (VMX Bi-slide, see \figurename~\ref{fig:setup}).
For a single acquired data set, we scan over the synthetic aperture in a raster pattern. We discuss future geometries that avoid physical scanning in the next section.
Objects to be imaged are placed $1.5$ meters away from the camera and are illuminated with a helium neon (HeNe) laser which emits a beam with a wavelength of $633$ nm.
We employ a spatial filter to make the laser intensity more uniform as well as to ensure sufficient illumination coverage on our objects.
A lens is placed immediately before the target to collect the illumination and form the Fourier transform on the aperture plane of the camera lens.

\begin{figure}[t!]%
\centering
\includegraphics[width=\linewidth]{simFace}
\caption{\textbf{Simulation of capturing a face $1000$ meters away.} We simulate image capture with an inexpensive $1200$ mm lens, $75$ mm aperture diameter ($f/16$), and a pixel pitch of $2~\mu$m. (a) Directly using the inexpensive $f/16$ lens in passive illumination results in a diffraction blur spot size of $17.8$ mm on the face, obliterating detail necessary to recognize the subject. (b) Using Fourier ptychography ($61\%$ overlap, $81$ images) to achieve a synthetic aperture of $300$ mm the diffraction spot size is reduced to $4.4$ mm. (c) Using the $\$150{,}000$ $215$ mm ($f/5.6$) lens yields a diffraction spot size of $6.2$ mm, $50\%$ larger than the diffraction realized using Fourier ptychography. Detail views of the three systems are shown in (d). In this simulation diffraction is the only source of blur, other factors in \tablename~\ref{tab:motivTab} are not considered.}%
\label{fig:simResults_face}%
\end{figure}

Lenses with small focal lengths can be easily (and cheaply) manufactured to have a proportionally large aperture. The maximum aperture of the lens that we use is $42$ mm.
In order to simulate building a cheap lens with a long focal length and small aperture, we stopped the lens down to $f/32$, creating a $2.3$ mm diameter aperture.
The diffraction spot size on the sensor is $49~\mu$m in diameter.
Given that the camera sensor has a pixel width of $2.2~\mu$m we expect to see a blur of roughly $20$ pixels.

Unique images are acquired by moving the camera to equidistant positions in the synthetic aperture plane via the translation stage.
At every grid position, we capture multiple images with different exposures to form a high dynamic range reconstruction.
By translating the camera, the shift in perspective causes the captured images to be misaligned.
For planar scenes a pair of misaligned images can be corrected by finding the homography relating the reference viewpoint (taken to be the center position) and the outlying view.
We accomplish this by finding fiducial markers of a checkerboard pattern affixed to the object, which is lit with incoherent light.
The translation stage is of high enough precision to allow the calibration step to be performed immediately before or after data capture.
The aligned high dynamic range images are used as inputs to the phase retrieval algorithm.

\Subsection{Recovered images}
We test our setup with three scenes. First, we deposit a fingerprint on a glass microscope slide and use consumer grade fingerprint powder to reveal the latent print. Our second scene contains a translucent water bottle label, and our third scene is a resolution target.
For the scenes containing the fingerprint and water bottle labels, we record a $17 \times 17$ grid of images with $81\%$ overlap between adjacent images, resulting in a synthetic aperture with a $9.3$ mm diameter (SAR=$4$).

\textbf{Fingerprint:} \figurename~\ref{fig:realFingerprint} shows the results of using our coherent camera array prototype to improve the resolution of a fingerprint.
With an aperture diameter of $2.3$ mm, little high spatial frequency information is transferred to the image plane in a single image, resulting in the blurry image in \figurename~\ref{fig:realFingerprint}(a).
The coherent camera array technique recovers a high resolution Fourier representation of the object, from which we recover both the high resolution image and the phase of the object (\figurename~\ref{fig:realFingerprint}(b)).
The wrapping in the recovered phase could be caused by the illumination wavefront, or by a tilt in the object with respect to the camera.
One consequence of stopping down the lens aperture is that we are able to fully open the aperture and record and image which does not exhibit any diffraction blur, \figurename~\ref{fig:realFingerprint}(c), which provides a useful comparison for our reconstruction.
Detail views of the three imaging scenarios are shown in \figurename~\ref{fig:realFingerprint}(d). 

\begin{figure}[t!]%
\centering
\includegraphics[width=3.3in]{experimentalSetup}%
\caption{\textbf{Overview of hardware configuration for data acquisition (side view).} From left to right: A helium neon laser passed through a spatial filter acts as the coherent illumination source. A focusing lens forms the Fourier transform of the transmissive object at the aperture plane of the camera's lens. The aperture acts as a bandpass filter of the Fourier transform and the signal undergoes an inverse Fourier transform as it is focused onto the camera's sensor. The camera (Point Grey Blackfly (BFLY-PGE-50A2M-CS) is mounted on a motorized $2$D stage to capture overlapping images.}%
\label{fig:setup}%
\vspace{-\baselineskip}
\end{figure}

\begin{figure*}[t]%
\centering
\includegraphics[width=\linewidth]{realResults_fingerprint}
\caption{\textbf{Experimental result: Resolving a fingerprint $4\times$ beyond the diffraction limit.} Using the hardware setup shown in \figurename~\ref{fig:setup}, we use a $75$ mm focal length lens to acquire images of a fingerprint $\sim 1.5$ m away from the camera. A fingerprint was pressed on a glass slide and fingerprint powder was used to make the ridges opaque. (a) Stopping down the aperture to a diameter of $2.34$ mm induces a diffraction spot size of $49~\mu$m on the sensor ($\sim 20$ pixels) and $990~\mu$m on the object. (b) We record a grid of $17\times17$ images with an overlap of $81\%$, resulting in a SAR which is $4$ times larger than the lens aperture. After running phase retrieval, we recover a high resolution magnitude image and the phase of the objects. (c) For comparison, we open the aperture to a diameter of $41$ mm to reduce the diffraction blur to the size of a pixel. (d) Comparing zoomed in regions of the observed, recovered, and comparison images shows that our framework is able to recover details which are completely lost in a diffraction limited system. }%
\label{fig:realFingerprint}%
\vspace{-\baselineskip}
\end{figure*}

\begin{figure*}[t]%
\centering
\includegraphics[width=\linewidth]{realResults_dasani}
\caption{\textbf{Experimental result: Resolving $4\times$ beyond the diffraction limit for a diffuse water bottle label.} Using the same parameters as in \figurename~\ref{fig:realFingerprint}, we image a diffuse water bottle label $\sim 1.5$ m away from the camera. The diffuse nature of the water bottle label results in laser speckle. In the observed center image (a), diffraction blur and laser speckle render the text illegible. Using Fourier ptychography (b) we are able to reduce the effect of speckle and remove diffraction revealing the text. In the comparison image (c) the text is clearly legible. Detail views in (d) show the improvement in image resolution when using Fourier ptychography.}%
\label{fig:realDasani}%
\vspace{-\baselineskip}
\end{figure*}

\begin{figure*}[t]%
\centering
\includegraphics[width=\linewidth]{realResult_USAF}
\caption{\textbf{Experimental result: Recovering a USAF target with varying SAR.} We capture a USAF resolution target $1.5$ meters away from the camera. For this experiment, we use a slightly different lens and image sensor (focal length = $75$ mm, pixel pitch = $3.75~\mu$m). (a) The camera is stopped down to $2.5$ mm which induces a $930~\mu$m blur on the resolution chart, limiting resolution to $1.26$ line pairs per millimeter (lp/mm). (b) We record a $23\times23$ grid of images with $72\%$ overlap, resulting in a SAR of $7.16$. Following phase retrieval, we are able to resolve features as small as $8.98$ lp/mm, a $7.12\times$ improvement in resolution. (c) We show the effect of varying the SAR on resolution. Using a subset of the captured images we vary the SAR from $2.12$ up to $7.16$. The gains in improvement closely track the SAR.}%
\label{fig:USAF}%
\vspace{-\baselineskip}
\end{figure*}

\textbf{Dasani label:} We next use the same setup to capture a translucent Dasani water bottle label, shown in \figurename~\ref{fig:realDasani}.
Unlike the fingerprint, the diffuse water bottle label has a random phase which produces a highly varying optical field at the image plane, which is reminiscent of laser speckle.
Speckle becomes more pronounced for small apertures (\figurename~\ref{fig:realDasani}(a)).
The recovered image in \figurename~\ref{fig:realDasani}(b) shows that the coherent camera array can recover details that are completely lost in a diffraction limited system, and the phase retrieval algorithm is able to handle the quickly varying random phase of the label.
Even with a modest increase to the SAR, individual letters are legible, as highlighted in the detailed crops in \figurename~\ref{fig:realDasani}(d). 
If we were to further increase the SAR, we could further reduce speckle-induced artifacts, as suggested by the raw open-aperture image in \figurename~\ref{fig:realDasani}(c).

\textbf{USAF target:} We use the same setup to capture a USAF target, but now with a different $75$ mm lens (Edmund Optics \#54-691) and a slightly different Blackfly camera (BFLY-PGE-13S2M-CS) with a pixel pitch of $3.75~\mu$m.
Using this setup, and an aperture diameter of $2.5$ mm, we expect a diffraction blur size of $48~\mu$m on the image sensor (approximately $12$ sensor pixels).
With the USAF target placed $1.5$ meters away from the camera, we expect a diffraction blur size of $970~\mu$m at the object plane.
We observe that the resolution of the USAF target is limited to $1.26$ line pairs per millimeter (lp/mm) in the center observed image (Group 0, Element 3 in \figurename~\ref{fig:USAF}a).
By acquiring a $23\times23$ grid of images, with $72\%$ overlap between neighboring images, we achieve an effective synthetic aperture ratio of $7.16$.
We show the results of such a coherent camera array reconstruction in \figurename~\ref{fig:USAF}(b), where the spatial recovered spatial resolution has increased to $8.98$ lp/mm (Group 3, Element 2), a $7.12\times$ improvement in resolution.

As discussed in Section~\ref{sec:simRecov}, varying the synthetic aperture ratio impacts the system resolution enhancement factor. 
This can be observed by varying the number of images used to recover the USAF target.
In \figurename~\ref{fig:USAF}(c) we present two regions of the target for a varying SAR of $1-7.16$.
As the SAR increases to $2.12$, $4.36$, $5.48$, and $7.16$ the realized resolution gains are $1.94$, $4.00$, $5.66$, and $7.12$ respectively, which closely tracks the expected behavior.

\textbf{Reproducible Research:} We are committed to reproducible research. Our codes and data sets may be found on our project webpage~\cite{projectWebpage} (\href{http://jrholloway.com/projects/towardCCA}{http://jrholloway.com/projects/towardCCA}).